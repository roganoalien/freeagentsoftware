const INDEX = (function(){

    let $btn_s,
        $section_array;

    const _initVars = () => {
        $btn_s = $('.select-btn');
        $section_array = ['cards', 'list', 'small-cards'];
    };

    const _initEvents = () => {
        $btn_s.on('click', function(){
            let _order = $(this).attr('data-order');
            $btn_s.removeClass('current');
            $(this).addClass('current');
            console.log(_order);
            UTILS.remove($section_array, _order);
            UTILS.add(_order);
            console.log('faltó diseño para cada tipo de vista');
        });
    };

    return {
        init : function(){
            _initVars();
            _initEvents();
        }
    }
})();