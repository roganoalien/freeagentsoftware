const MAIN_NAV = (function(){

    const _initVars = () => {};

    const _initEvents = () => {};

    return{
        init: function(){
            _initVars();
            _initEvents();
        }
    }
})();