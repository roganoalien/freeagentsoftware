const UTILS = (function(){

    let $section = $('.section-content');

    const _addClass = (name) => {
        $section.addClass(name);
    };

    const _removeClass = (object, name) => {
        for(let i = 0; i < object.length; i++){
            $section.removeClass(object[i]);
        }
    };

    return{
        add : function(name){
            _addClass(name);
        },
        remove : function(object, name){
            _removeClass(object, name);
        }
    }
})();